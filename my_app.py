import pymongo
from flask import request, flash
import json
import requests
import time
from datetime import datetime, date
from flask import Flask, render_template
# Api_key = /mQQHQ6Ec03BmFXK1W1x0GSHW5K1hqSFm
# lNmieAnXTUZV3beA6e0mCfAg94vDwbRf

from routes.front_routes import front
from routes.api_routes import back

app = Flask(__name__)
app.secret_key = "secret key"

app.register_blueprint(front, url_prefix='')
app.register_blueprint(back, url_prefix='')

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=3000)
