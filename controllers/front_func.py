from datetime import time
import time
from flask import request, flash, render_template
from controllers.weather_api import current_con, get_five


# @app.route('/cur_we', methods=["GET", "POST"])
def current_weather():
    if request.method == "POST":
        city = request.form["city"]
        country = request.form["country"]
        if city == '' or country == '':
            flash("-  Fields can't be empty  -")
            return render_template('current_weather.html')
        weather_dict = current_con(request.form["city"], request.form["country"])
        temperature = weather_dict["temperature"]["metric"]
        humidity = weather_dict["humidity"]
        weaher_icon = "static/" + weather_dict["weatherText"].replace(" ", "").replace('/', '').lower()
        if 'cloud' in weaher_icon:
            pic = "static/Cloudy.png"
        elif 'rain' in weaher_icon:
            pic = "static/Rain.png"
        elif 'snow' in weaher_icon:
            pic = "static/Snow.png"
        elif 'sun' in weaher_icon:
            pic = "static/Sunny.png"
        elif 'clear' in weaher_icon:
            pic = "static/Clear.png"

        icon = True
        return render_template('current_weather.html', template_temp=temperature, template_humidity=humidity,
                               template_icon=icon, template_pic=pic)
    else:
        return render_template('current_weather.html')


# @app.route('/')
def home():
    return render_template('home.html')

#@app.route('/five_we', methods=["GET", "POST"])
def five_weather():
    if request.method == "POST":
        city = request.form["city"]
        country = request.form["country"]
        if city == '' or country == '':
            flash("-  Fields can't be empty  -")
            return render_template('five_days_weather.html')
        weather_dict = get_five(request.form["city"], request.form["country"])

        info = []
        for i in range(5):
            info.append({"day_date": time.strftime('%Y-%m-%d', time.localtime(weather_dict["forecast"][i]["time"])), "d_temperature": weather_dict["forecast"][i]["day"]["temperature"], "d_pic": "static/" + weather_dict["forecast"][i]["day"]["weatherText"].replace(" ", "").replace('/', '') + ".png", "n_temperature": weather_dict["forecast"][i]["night"]["temperature"], "n_pic": "static/" + weather_dict["forecast"][i]["night"]["weatherText"].replace(" ", "").replace('/', '') + ".png"})

        #day_date = time.strftime('%Y-%m-%d', time.localtime(weather_dict["forecast"][0]["time"]))
        #temperature = weather_dict["forecast"][0]["day"]["temperature"]
        #pic = "static/" + weather_dict["forecast"][0]["day"]["weatherText"].replace(" ", "").replace('/', '') + ".png"
        icon = True
        print(info)
        return render_template('five_days_weather.html', tmp_info=info, template_icon=icon)
        #return render_template('five_days_weather.html', template_temp=temperature, template_pic=pic, template_icon=icon, template_time=day_date)
    else:
        return render_template('five_days_weather.html')
