import json
from datetime import time, datetime, date
import time
import pymongo
import requests
import os

#MONGO_CON = "mongodb://10.101.111.101:27017"
MONGO_CON = os.environ.get('MONGO_CON')
API_KEY = os.environ.get('API_KEY', "lNmieAnXTUZV3beA6e0mCfAg94vDwbRf")


#@app.route('/api/v1/curcon/<city>/<country>')
def get_city_id(city, country):
    with pymongo.MongoClient(MONGO_CON) as myclient:
        mydb = myclient["weather"]
        mycol = mydb["city_id"]
        myquery = {'city': city, 'country': country}
        mydoc = mycol.find(myquery)
        print(mycol.count_documents(myquery))
        if mycol.count_documents(myquery) > 0:
            for x in mydoc:
                city_id = x['id']
                print(x['id'])
                return city_id
        else:
            print("inelse")
            param_dict = {
                "apikey": API_KEY,
                "q": city
            }
            url = "http://dataservice.accuweather.com/locations/v1/cities/search"
            r = requests.get(url, params=param_dict)
            k = json.dumps(r.json())
            data = json.loads(k)
            for i in data:
                if i["Country"]["ID"] == country:
                    print("key is present")
                    #print(i["Key"])
                    city_id = i["Key"]
            mydict = {'city': city, 'country': country, 'id': city_id}
            x = mycol.insert_one(mydict)
    print(city_id)
    return city_id


#@app.route("/api/v1/curcon", methods=["GET"])
def current_con(city, country):
    #city = request.args.get('city')
    #country = request.args.get('country')
    city_id = get_city_id(city, country)
    print(city_id)
    with pymongo.MongoClient(MONGO_CON) as myclient:
        mydb = myclient["weather"]
        mycol = mydb["current_condition"]

        myquery = {'city': city, 'country': country}
        if mycol.count_documents(myquery) > 0:
            row = mycol.find(myquery, {"_id": 0})
            for i in row:
                mydict = i
                time_in_db = i["time"]
            print(time_in_db)
            print(time.time())
            time_dif = time.time() - time_in_db
            if time_dif > 3600:
                mycol.delete_one(myquery)
                param_dict = {
                    "apikey": API_KEY,
                    "details": "true"
                }
                url = "http://dataservice.accuweather.com/currentconditions/v1/{}".format(city_id)
                r = requests.get(url, params=param_dict)
                k = json.dumps(r.json())
                data = json.loads(k)
                # Iterating through the json
                # list
                for i in data:
                    # print(i)
                    print(i["EpochTime"])
                    print(i["WeatherText"])
                    print(i["Temperature"]["Metric"]["Value"])
                    print(i["Temperature"]["Imperial"]["Value"])
                    print(i["RelativeHumidity"])
                    mydict = {"city": city, "country": country, "time": i["EpochTime"], "weatherText": i["WeatherText"],
                              "temperature": {"imperial": i["Temperature"]["Imperial"]["Value"],
                                              "metric": i["Temperature"]["Metric"]["Value"]},
                              "humidity": i["RelativeHumidity"]}
                    x = mycol.insert_one(mydict)
            print(time_dif)
            print(row)
        else:
            param_dict = {
                "apikey": API_KEY,
                "details": "true"
            }
            url = "http://dataservice.accuweather.com/currentconditions/v1/{}".format(city_id)
            r = requests.get(url, params=param_dict)
            k = json.dumps(r.json())
            data = json.loads(k)
            for i in data:
                # print(i)
                print(i["EpochTime"])
                print(i["WeatherText"])
                print(i["Temperature"]["Metric"]["Value"])
                print(i["Temperature"]["Imperial"]["Value"])
                print(i["RelativeHumidity"])
                mydict = {"city": city, "country": country, "time": i["EpochTime"], "weatherText": i["WeatherText"],
                          "temperature": {"imperial": i["Temperature"]["Imperial"]["Value"],
                                          "metric": i["Temperature"]["Metric"]["Value"]}, "humidity": i["RelativeHumidity"]}
                x = mycol.insert_one(mydict)

    if '_id' in mydict:
        del mydict['_id']
    #print(mydict)
    print(mydict)
    return mydict


#@app.route("/api/v1/curcon5", methods=['GET'])
def get_five(city, country):
    #city = request.args.get('city')
    #country = request.args.get('country')
    city_id = get_city_id(city, country)
    print(str(city_id), " !!!")
    with pymongo.MongoClient(MONGO_CON) as myclient:
        mydb = myclient["weather"]
        mycol = mydb["five_days"]

        myquery = {'city': city, 'country': country}
        row = mycol.find(myquery, {"_id": 0})
        for i in row:
            print(i["forecast"])
        print("-----")
        print(mycol.count_documents(myquery))
        print("-----")
        if mycol.count_documents(myquery) > 0:
            row = mycol.find(myquery, {"_id": 0})
            for i in row:
                my_dict = i
                time_in_db = i["forecast"][0]["time"]
            print("-----")
            print(time_in_db)
            print("-----")
            my_time = datetime.strptime(str(date.today()), "%Y-%m-%d")
            my_time2 = time.strftime('%Y-%m-%d', time.localtime(time_in_db))
            my_time1 = datetime.strptime(my_time2, "%Y-%m-%d")
            res = (my_time - my_time1).days
            print(my_time)
            print(str(time_in_db) + " time in db")
            print(my_time1)
            print(res)
            if res > 0:
                print("connecting API")
                mycol.delete_one(myquery)
                param_dict = {
                    "apikey": API_KEY,
                    "metric": "true"
                }
                url = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/{}".format(city_id)
                r = requests.get(url, params=param_dict)
                k = json.dumps(r.json())
                data = json.loads(k)
                x = []
                for i in data["DailyForecasts"]:
                    print(i["EpochDate"])
                    print(i["Temperature"]["Minimum"]["Value"])
                    print(i["Temperature"]["Maximum"]["Value"])
                    print(i["Day"]["IconPhrase"])
                    print(i["Night"]["IconPhrase"])
                    dic = {"time": i["EpochDate"], "day": {"weatherText": i["Day"]["IconPhrase"],
                                                           "temperature": i["Temperature"]["Maximum"]["Value"]},
                           "night": {"weatherText": i["Night"]["IconPhrase"],
                                     "temperature": i["Temperature"]["Minimum"]["Value"]}}
                    x.append(dic)
                my_dict = {"city": city, "country": country, "forecast": x}
                mycol.insert_one(my_dict)
            print(row)
        else:
            print("connecting API")
            param_dict = {
                "apikey": API_KEY,
                "metric": "true"
            }
            url = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/{}".format(city_id)
            r = requests.get(url, params=param_dict)
            k = json.dumps(r.json())
            data = json.loads(k)
            x = []
            for i in data["DailyForecasts"]:
                print(i["EpochDate"])
                print(i["Temperature"]["Minimum"]["Value"])
                print(i["Temperature"]["Maximum"]["Value"])
                print(i["Day"]["IconPhrase"])
                print(i["Night"]["IconPhrase"])
                dic = {"time": i["EpochDate"], "day": {"weatherText": i["Day"]["IconPhrase"],
                                                       "temperature": i["Temperature"]["Maximum"]["Value"]},
                       "night": {"weatherText": i["Night"]["IconPhrase"],
                                 "temperature": i["Temperature"]["Minimum"]["Value"]}}
                x.append(dic)
            my_dict = {"city": city, "country": country, "forecast": x}
            print(my_dict)
            mycol.insert_one(my_dict)
    print(my_dict)
    if '_id' in my_dict:
        del my_dict['_id']
    print(my_dict)
    return my_dict
