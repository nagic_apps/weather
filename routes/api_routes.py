from flask import Blueprint
from controllers.weather_api import get_city_id, current_con, get_five
back = Blueprint('back', __name__)

back.route('/v1/curcon/<city>/<country>')(get_city_id)
back.route("/v1/curcon", methods=["GET"])(current_con)
back.route("/v1/curcon5", methods=['GET'])(get_five)
