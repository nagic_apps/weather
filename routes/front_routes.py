from flask import Blueprint
from controllers.front_func import current_weather, home, five_weather

front = Blueprint('front', __name__)

front.route('/cur_we', methods=["GET", "POST"])(current_weather)
front.route('/')(home)
front.route('/five_we', methods=["GET", "POST"])(five_weather)
